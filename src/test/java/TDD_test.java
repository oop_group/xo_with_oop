/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author kitti
 */
public class TDD_test {

    public TDD_test() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    //add(1,2) -> 3
    @Test
    public void testAdd_1_2is3() {
        assertEquals(3, Example.add(1, 2));
    }

    //add(3,4) -> 7
    @Test
    public void testAdd_3_4is7() {
        assertEquals(7, Example.add(3, 4));
    }

    //add(20,22) -> 42
    @Test
    public void testAdd_20_22is42() {
        assertEquals(42, Example.add(20, 22));
    }

    //h: hammer, p: paper, s: scissors
    //chub(char player1, char player2) -> "p1", "p2", "draw"
    @Test
    public void testChub_p1p_p2p_is_draw(){
        assertEquals("draw", Example.chub('p','p'));
    }
    
    @Test
    public void testChub_p1h_p2h_is_draw(){
        assertEquals("draw", Example.chub('h','h'));
    }
    
    @Test
    public void testChub_p1s_p2s_is_draw(){
        assertEquals("draw", Example.chub('s','s'));
    }
    
    @Test
    public void testChub_p1s_p2p_is_p1(){
        assertEquals("p1", Example.chub('s','p'));
    }
    
    	@Test
     public void testchup_p1_h_p2_s_is_p1(){
       assertEquals("p1", Example.chub('h','s'));
    }
     
      @Test
     public void testchup_p1_p_p2_h_is_p1(){
       assertEquals("p1", Example.chub('p','h'));
    }
     
     //chup p2 win
     @Test
     public void testchup_p1_h_p2_p_is_p1(){
       assertEquals("p2", Example.chub('h','p'));
    }
     
     @Test
     public void testchup_p1_p_p2_s_is_p1(){
       assertEquals("p2", Example.chub('p','s'));
    }
     
      @Test
     public void testchup_p1_s_p2_h_is_p1(){
       assertEquals("p2", Example.chub('s','h'));
    }
}
