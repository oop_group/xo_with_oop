/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author kitti
 */
public class XOProgramTest {

    public XOProgramTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testCheckPlayerOCol1Win() {
        char table[][] = {{'O', '-', '-'},
        {'O', '-', '-'},
        {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;

        assertEquals(true, XO_Program.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckPlayerOCol2Win() {
        char table[][] = {{'-', 'O', '-'},
        {'-', 'O', '-'},
        {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int col = 2;

        assertEquals(true, XO_Program.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckPlayerOCol3Win() {
        char table[][] = {{'-', '-', 'O'},
        {'-', '-', 'O'},
        {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 3;

        assertEquals(true, XO_Program.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckPlayerOCol1lose() {

        char table[][] = {{'O', '-', '-'},
        {'X', '-', '-'},
        {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;

        assertEquals(false, XO_Program.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckPlayerOCol2lose() {
        char table[][] = {{'-', 'O', '-'},
        {'-', 'X', '-'},
        {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int col = 2;

        assertEquals(false, XO_Program.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckPlayerOCol3lose() {
        char table[][] = {{'-', '-', 'O'},
        {'-', '-', 'X'},
        {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 3;

        assertEquals(false, XO_Program.checkVertical(table, currentPlayer, col));
    }

    //=============================================================
    @Test
    public void testCheckPlayerXCol1Win() {

        char table[][] = {{'X', '-', '-'},
        {'X', '-', '-'},
        {'X', '-', '-'}};
        char currentPlayer = 'X';
        int col = 1;

        assertEquals(true, XO_Program.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckPlayerXCol2Win() {
        char table[][] = {{'-', 'X', '-'},
        {'-', 'X', '-'},
        {'-', 'X', '-'}};
        char currentPlayer = 'X';
        int col = 2;

        assertEquals(true, XO_Program.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckPlayerXCol3Win() {
        char table[][] = {{'-', '-', 'X'},
        {'-', '-', 'X'},
        {'-', '-', 'X'}};
        char currentPlayer = 'X';
        int col = 3;

        assertEquals(true, XO_Program.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckPlayerXCol1lose() {

        char table[][] = {{'X', '-', '-'},
        {'X', '-', '-'},
        {'O', '-', '-'}};
        char currentPlayer = 'X';
        int col = 1;

        assertEquals(false, XO_Program.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckPlayerXCol2lose() {
        char table[][] = {{'-', 'X', '-'},
        {'-', 'X', '-'},
        {'-', 'O', '-'}};
        char currentPlayer = 'X';
        int col = 2;

        assertEquals(false, XO_Program.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckPlayerXCol3lose() {
        char table[][] = {{'-', '-', 'X'},
        {'-', '-', 'X'},
        {'-', '-', 'O'}};
        char currentPlayer = 'X';
        int col = 3;

        assertEquals(false, XO_Program.checkVertical(table, currentPlayer, col));
    }

    //=============================================================
    //=============================================================
    @Test
    public void testCheckPlayerORow1Win() {

        char table[][] = {{'O', 'O', 'O'},
        {'-', '-', '-'},
        {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 1;

        assertEquals(true, XO_Program.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckPlayerORow2Win() {

        char table[][] = {{'-', '-', '-'},
        {'O', 'O', 'O'},
        {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 2;

        assertEquals(true, XO_Program.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckPlayerORow3Win() {

        char table[][] = {{'-', '-', '-'},
        {'-', '-', '-'},
        {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        int row = 3;

        assertEquals(true, XO_Program.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckPlayerXRow1Win() {

        char table[][] = {{'X', 'X', 'X'},
        {'-', '-', '-'},
        {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 1;

        assertEquals(true, XO_Program.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckPlayerXRow2Win() {

        char table[][] = {{'-', '-', '-'},
        {'X', 'X', 'X'},
        {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 2;

        assertEquals(true, XO_Program.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckPlayerXRow3Win() {

        char table[][] = {{'-', '-', '-'},
        {'-', '-', '-'},
        {'X', 'X', 'X'}};
        char currentPlayer = 'X';
        int row = 3;

        assertEquals(true, XO_Program.checkHorizontal(table, currentPlayer, row));
    }

    //=============================================================
    @Test
    public void testCheckPlayerORow1lose() {

        char table[][] = {{'X', 'O', 'O'},
        {'-', '-', '-'},
        {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 1;

        assertEquals(false, XO_Program.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckPlayerORow2lose() {

        char table[][] = {{'-', '-', '-'},
        {'O', 'X', 'O'},
        {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 2;

        assertEquals(false, XO_Program.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckPlayerORow3lose() {

        char table[][] = {{'-', '-', '-'},
        {'-', '-', '-'},
        {'O', 'O', 'X'}};
        char currentPlayer = 'O';
        int row = 3;

        assertEquals(false, XO_Program.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckPlayerXRow1lose() {

        char table[][] = {{'X', 'O', 'X'},
        {'-', '-', '-'},
        {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 1;

        assertEquals(false, XO_Program.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckPlayerXRow2lose() {

        char table[][] = {{'-', '-', '-'},
        {'X', 'X', 'O'},
        {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 2;

        assertEquals(false, XO_Program.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckPlayerXRow3lose() {

        char table[][] = {{'-', '-', '-'},
        {'-', '-', '-'},
        {'O', 'X', 'X'}};
        char currentPlayer = 'X';
        int row = 3;

        assertEquals(false, XO_Program.checkHorizontal(table, currentPlayer, row));
    }

    //=============================================================
    //=============================================================
    @Test
    public void testCheckPlayerODaigonalLefttOWin() {

        char table[][] = {{'O', '-', '-'},
        {'-', 'O', '-'},
        {'-', '-', 'O'}};

        char currentPlayer = 'O';

        assertEquals(true, XO_Program.checkXL(table, currentPlayer));

    }

    @Test
    public void testCheckPlayerODaigonalLefttOlose() {

        char table[][] = {{'O', '-', '-'},
        {'-', 'X', '-'},
        {'-', '-', 'O'}};

        char currentPlayer = 'O';

        assertEquals(false, XO_Program.checkXL(table, currentPlayer));

    }
    //=============================================================

    @Test
    public void testCheckPlayerODaigonalLefttXWin() {

        char table[][] = {{'X', '-', '-'},
        {'-', 'X', '-'},
        {'-', '-', 'X'}};

        char currentPlayer = 'X';

        assertEquals(true, XO_Program.checkXL(table, currentPlayer));

    }

    @Test
    public void testCheckPlayerODaigonalLefttXlose() {

        char table[][] = {{'X', '-', '-'},
        {'-', 'X', '-'},
        {'-', '-', 'O'}};

        char currentPlayer = 'X';

        assertEquals(false, XO_Program.checkXL(table, currentPlayer));

    }

    //=============================================================
    //=============================================================
    @Test
    public void testCheckPlayerOWin() {

        char table[][] = {{'O', '-', '-'},
        {'O', '-', '-'},
        {'O', '-', '-'}};

        char currentPlayer = 'O';
        int col = 1;
        int row = 1;

        assertEquals(true, XO_Program.checkWin(table, currentPlayer, col, row));
    }

    @Test
    public void testCheckPlayerXWin() {

        char table[][] = {{'X', '-', '-'},
        {'X', '-', '-'},
        {'X', '-', '-'}};

        char currentPlayer = 'X';
        int col = 1;
        int row = 1;

        assertEquals(true, XO_Program.checkWin(table, currentPlayer, col, row));
    }

    //=============================================================
    @Test
    public void testCheckPlayerOLose() {

        char table[][] = {{'O', '-', '-'},
        {'X', '-', '-'},
        {'O', '-', '-'}};

        char currentPlayer = 'O';
        int col = 1;
        int row = 1;

        assertEquals(false, XO_Program.checkWin(table, currentPlayer, col, row));
    }

    @Test
    public void testCheckPlayerXLose() {

        char table[][] = {{'X', '-', '-'},
        {'O', '-', '-'},
        {'X', '-', '-'}};

        char currentPlayer = 'X';
        int col = 1;
        int row = 1;

        assertEquals(false, XO_Program.checkWin(table, currentPlayer, col, row));
    }

    //=============================================================
    //=============================================================
    @Test
    public void testCheckDraw() {

        int count = 9;

        assertEquals(true, XO_Program.checkDraw(count));
    }

    //=============================================================
    //=============================================================
    @Test
    public void testCheckX_1() {

        char table[][] = {{'X', '-', '-'},
        {'-', 'X', '-'},
        {'-', '-', 'X'}};

        char currentPlayer = 'X';

        assertEquals(true, XO_Program.checkX(table, currentPlayer));
    }

    @Test
    public void testCheckX_2() {

        char table[][] = {{'-', '-', 'X'},
        {'-', 'X', '-'},
        {'X', '-', '-'}};

        char currentPlayer = 'X';

        assertEquals(true, XO_Program.checkX(table, currentPlayer));
    }

    //=============================================================
    @Test
    public void testCheckO_1() {

        char table[][] = {{'O', '-', '-'},
        {'-', 'O', '-'},
        {'-', '-', 'O'}};

        char currentPlayer = 'O';

        assertEquals(true, XO_Program.checkX(table, currentPlayer));
    }

    @Test
    public void testCheckO_2() {

        char table[][] = {{'-', '-', 'O'},
        {'-', 'O', '-'},
        {'O', '-', '-'}};

        char currentPlayer = 'O';

        assertEquals(true, XO_Program.checkX(table, currentPlayer));
    }
}
